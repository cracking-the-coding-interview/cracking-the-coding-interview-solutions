Cracking the Coding Interview Solutions
========================================

# Overview

Hi!  My name's Jeremy Reed, I've bought a copy of
"Cracking the Coding Interview".  I will be solving each of
the 189 programming questions, and sharing a link to
repositories containing solutions to each of the programming
questions here.

Generally each solution will contain:
1. A README file that contains the following:
	1. A problem statement.
	2. A list of any questions that I'd be asking if this
	were a real interview question.
	3. A description of my solution, and a discussion of
	how I chose my solution.
2. Code implementing and testing my solution.
	- I can use any resource I want, as long as that
	resource does not include a solution to the programming
	question, or a discussion of that solution.
		- These resources must be documented.

This document will be updated as I complete the problems.

### Table of contents:
1. [Overview](https://gitlab.com/cracking-the-coding-interview/cracking-the-coding-interview-solutions#overview)
2. [Solutions](https://gitlab.com/cracking-the-coding-interview/cracking-the-coding-interview-solutions#solutions)
3. [Contact](https://gitlab.com/cracking-the-coding-interview/cracking-the-coding-interview-solutions#contact)
4. [License](https://gitlab.com/cracking-the-coding-interview/cracking-the-coding-interview-solutions#license)

## Solutions

1. Chapter 1
	1. [Is Unique](https://gitlab.com/cracking-the-coding-interview/is-unique)
	2. [Check Permutations](https://gitlab.com/cracking-the-coding-interview/check-permutations)
	3. [URLify](https://gitlab.com/cracking-the-coding-interview/urlify)
	4. [Palindrome Permutation](https://gitlab.com/cracking-the-coding-interview/palindrome-permutation)
	5. [One Away](https://gitlab.com/cracking-the-coding-interview/one-away)
	6. [String Compression](https://gitlab.com/cracking-the-coding-interview/string-compression)


## Contact
	Jeremy M. Reed
	jeremy.reed.professional@gmail.com

## License

Licensed under the MIT License.
